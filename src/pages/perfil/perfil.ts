import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {AboutPage} from "../about/about";

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  items = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = [
      {
        'title': 'Web de Couriers',
        'icon': 'help',
        'flecha': 'arrow-dropright',
        'color': '#b2b2b2',
        'flechacolor': '#4b873e'
      },
      {
        'title': 'Chatear con soporte',
        'icon': 'chatbubbles',
        'flecha': 'arrow-dropright',
        'color': '#b2b2b2',
        'flechacolor': '#4b873e'
      },
      {
        'title': 'Contraseña',
        'icon': 'lock',
        'flecha': 'arrow-dropright',
        'color': '#b2b2b2',
        'flechacolor': '#4b873e'

      }
    ]
  }
  siguientePag(item){
    this.navCtrl.push(AboutPage,{item: item});
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

}
