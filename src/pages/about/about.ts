import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  lista: Array<any> = [
    {
      titulo: "Prueba1",
      colaboradorId: "Ticono Rodriguez",
      fechaHoraInicio: "10/05/19 | 15:03",
      fechaHoraEntrega: "13/05/19 | 17:30",
      tipoServicioId: "Lavado de interiores",
      precio: "10,50 €",
      clienteId: "Jaime Rincon",
      estadoId: "En Proceso",
      especificaciones: "Enjuagar con suavitel y cloro",
      direccionSalida: "Av. de Alberto de Alcocer, 42, Madrid, Spain",
      direccionEntrega: "Calle Bausá, 19, Barcelona, Spain "
    },
    {
      titulo: "Prueba2",
      colaboradorId: 11,
      fechaHoraInicio: "7/01/19",
      fechaHoraEntrega: "9/01/19",
      tipoServicioId: 31,
      precio: "10,50 €",
      clienteId: 4,
      estadoId: 3,
      especificaciones: "pruebadecarddinamicoqw4rwqetrqwet",
      direccionSalida: "Av. de Alberto de Alcocer, 42, Madrid, Spain",
      direccionEntrega: "Calle Bausá, 19, Barcelona, Spain "
    },
    {
      titulo: "Prueba3",
      colaboradorId: 19,
      fechaHoraInicio: "3/11/19",
      fechaHoraEntrega: "13/11/19",
      tipoServicioId: 7,
      precio: "10,50 €",
      clienteId: 22,
      estadoId: 1,
      especificaciones: "pruebadecarddinamico123456",
      direccionSalida: "Av. de Alberto de Alcocer, 42, Madrid, Spain",
      direccionEntrega: "Calle Bausá, 19, Barcelona, Spain "
    }
  ];

  constructor(public navCtrl: NavController) {

  }

}
