import {Component} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';

declare let google;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  map: any;
  colaborador: any;

  constructor(public navCtrl: NavController, private platform: Platform) {
    platform.ready().then(() => {
      this.initMap();
    });
  }

  initMap() {
    let mapEle = document.getElementById('map');
    let mapEle2=document.getElementById('map2');
    if (navigator.geolocation) {
      let app = this;
      navigator.geolocation.getCurrentPosition(function (position) {
        app.map = new google.maps.Map(mapEle, {
          zoom: 19,
          center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
        });
        let image = {
          url: '/assets/imgs/location_colaborador.png',
          size: new google.maps.Size(24, 24)
        };
        let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        app.colaborador = new google.maps.Marker({
          position: latlng,
          map: app.map,
          icon: image,
          animation: google.maps.Animation.DROP,
          title: 'Su ubicación'
        });
      });

      navigator.geolocation.watchPosition(function (position) {
        if (app.colaborador !== undefined)
          app.colaborador.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
        app.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));//hasta aqui

      }, function (error) {
      }, {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      });
    }
    if (navigator.geolocation) {
      let app = this;
      navigator.geolocation.getCurrentPosition(function (position) {
        app.map = new google.maps.Map(mapEle2, {
          zoom: 19,
          center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
        });
        let image = {
          url: '/assets/imgs/location_colaborador.png',
          size: new google.maps.Size(24, 24)
        };
        let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        app.colaborador = new google.maps.Marker({
          position: latlng,
          map: app.map,
          icon: image,
          animation: google.maps.Animation.DROP,
          title: 'Su ubicación'
        });
      });

      navigator.geolocation.watchPosition(function (position) {
        if (app.colaborador !== undefined)
          app.colaborador.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
        app.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));//hasta aqui

      }, function (error) {
      }, {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      });
    }
  }
}
